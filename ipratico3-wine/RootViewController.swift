//
//  RootViewController.swift
//  ipratico3-wine
//
//  Created by Matteo Spreafico on 9/3/19.
//  Copyright © 2019 iPratico srl. All rights reserved.
//

import UIKit

class RootViewController: UINavigationController {
    struct DevNoLogin {
        let ws:String = "ws://cloud3.ipraticocloud.com:4984/db"
        let bucket:String = "ipratico_store"
        let user:String = "emanuele"
        let password:String = "1234Goldens"
        let location:String = "lct_9256" //lct_9255
        let company:String = "cmp_3269"
        let otherChannels:[String] = ["ipratico_eat"]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.red
        self.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    override init(rootViewController: UIViewController) {
        super.init(navigationBarClass: UINavigationBar.self, toolbarClass: nil)
        viewControllers = [rootViewController]
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let dnl = DevNoLogin()
        openDatabaseConnection(ws: dnl.ws, bucket: dnl.bucket, user: dnl.user, password: dnl.password, location: dnl.location, company: dnl.company, otherChannels: dnl.otherChannels)
    }
    
    
    
    func openDatabaseConnection(ws:String,bucket:String, user:String, password:String, location:String, company:String, otherChannels:[String]){
        var channels:[String]=[String]()
        channels.append(location)
        channels.append(company)
        for c in otherChannels{
            if(c != location && c != company){
                channels.append(c)
            }
        }
        WineManager.shared.openDatabase(endPoint: ws, bucket: bucket, user: user, password: password, channels: channels, isReplication: true, accessToken: "")
        
    }
    
    
}
