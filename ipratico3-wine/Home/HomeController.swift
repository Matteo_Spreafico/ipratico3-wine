// HomeController.swift
// ipratico3-wine
//
// Created by Matteo Spreafico on 9/4/19.
//Copyright © 2019 iPratico srl. All rights reserved.
//

import UIKit
import iPraticoComponents
import GreenLineIOs

class HomeController: UIViewController {
    
    var router:HomeRouter? = nil
    let presenter: HomePresenter = HomePresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.router = HomeRouter(controller: self)
        WineManager.shared.delegate = self
        self.presenter.delegate = self
        setupInterface()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.presenter.getDataSource()
        createGradientLayer()
    }
    
    func setupInterface(){
        self.view.backgroundColor = .purple 
    }
    
    
    


    
}
extension HomeController: WineManagerDelegate{
    func databaseIsReady() {
        print("database is ready")
        self.presenter.getDataSource()
    }
    
    
}

extension HomeController: HomePresenterDelegate{
    
}

/// UTILS
extension HomeController {
    func createGradientLayer() {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor.iPratico.store.seaStart.cgColor,UIColor.iPratico.store.seaEnd.cgColor]
        
        self.view.layer.addSublayer(gradientLayer)
    }
}

