// HomePresenter.swift
// ipratico3-wine
//
// Created by Matteo Spreafico on 9/4/19.
//Copyright © 2019 iPratico srl. All rights reserved.
//
import GreenLineIOs
protocol  HomePresenterDelegate: AnyObject{
}
class HomePresenter{
    weak var delegate: HomePresenterDelegate? = nil
    var products = [GLProduct]()
    init() {
    }
    
    func getDataSource(){
        products = GLProduct.getAll()
        print(products)
    }
}
