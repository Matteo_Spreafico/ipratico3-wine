// HomeRouter.swift
// ipratico3-wine
//
// Created by Matteo Spreafico on 9/4/19.
//Copyright © 2019 iPratico srl. All rights reserved.
//

import UIKit

class HomeRouter{
    internal weak var controller:HomeController!
    
    init(controller: HomeController) {
        self.controller = controller
    }
    func dismissController(){
        self.controller.dismiss(animated: true, completion: nil)
    }
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}

