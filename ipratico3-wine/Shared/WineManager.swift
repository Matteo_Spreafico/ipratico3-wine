//
//  StoreManager.swift
//  Store
//
//  Created by Emanuele Cionini on 15/09/18.
//  Copyright © 2018 iPratico. All rights reserved.
//
import GreenLineIOs
import UserNotifications

/*
 struct SearchProductCategoryPLU {
 var categoryName:String
 var productName:String
 var plu:String
 var product:GLProduct
 var isModular:Bool?
 var price:Double
 var productPrices:GLProductPrices?
 }*/
protocol WineManagerDelegate:AnyObject{
    func databaseIsReady()
}

public var WM =  WineManager.shared
public class WineManager{
    static let shared = WineManager()
    var isAlertReplicationActive:Bool = true
    
    private var isReplicationError:Bool = false
    private var timerReplicationError:Timer? = nil
    weak var delegate:WineManagerDelegate? = nil
    
    
    
    //var selectedTable:GLTable?
    // Initialization
    private init() {
        GreenLineManager.sharedInstance.delegate = self
        //configureNukeCache()
        
        //Associo il delegato dello store al notificationCenter

        
    }
    
    func openDatabase(endPoint:String, bucket:String, user:String, password:String, channels:[String], isReplication:Bool, accessToken:String){
        GreenLineManager.sharedInstance.delegate = self
        GreenLineManager.sharedInstance.openDatabase(syncEndpoint: endPoint, bucket: bucket, user: user, password: password, channels: channels, replication: isReplication,accessToken: accessToken)
    }
    
    func configureNukeCache() {
        //NUKE CHACHE CONFIGURATION
        //DataLoader.sharedUrlCache.diskCapacity = 100
        //DataLoader.sharedUrlCache.memoryCapacity = 0
    }
    

}
extension WineManager: GreenLineManagerDelegate{
    public func GreenLineActivityDismissed() {
        print("test")
        self.delegate?.databaseIsReady()
    }
    public func GreenLineDatabaseHasBeenOpened() {
        print("test2")
        self.delegate?.databaseIsReady()
    }
    public func GreenLineManagerReachability(reachable:Bool, isWifi:Bool){
        
    }
    public func GreenLineManagerChange(bucket:String, activity:replicationStatusDB, progressComplete:UInt64, progressTotal:UInt64, error:NSError?){
        if(isAlertReplicationActive){
            if(error != nil){
                //self.startTimerError(message: error!.localizedDescription, category: LocalNotification.Category.replicationError)
            }else if(activity == .offline){
                //self.startTimerError(message: "device.offline".getLocalization, category: LocalNotification.Category.replicationError)
            }else if(activity == .stopped){
            }else if(activity == .idle){
                if(isReplicationError){
                    isReplicationError = false
                }else if(self.timerReplicationError != nil){
                    self.timerReplicationError!.invalidate()
                    self.timerReplicationError = nil
                }
            }
        }
    }
    public func GreenLineManagerCreateNewDatabase(bucket:String){
        
    }
    public func GreenLineManagerEndSyncNewDatabase(bucket:String){
        print("GreenLineManagerEndSyncNewDatabase")
    }
    
    func startTimerError(message:String, category: String){
        if(!isReplicationError){
            timerReplicationError = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { (timer) in
                self.isReplicationError = true
                self.timerReplicationError?.invalidate()
                self.timerReplicationError = nil
            })
        }
    }
}
